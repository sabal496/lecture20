package com.example.lecture19.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.lecture19.Api.RequestApi
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.Interfaces.callbackid
import com.example.lecture19.R
import kotlinx.android.synthetic.main.activity_create_user.*
import kotlinx.android.synthetic.main.activity_details.view.*

class CreateUser : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)
        init()
    }

    private fun init(){

        createuserbtn.setOnClickListener(){
            val name=name.text
            val job=job.text
            if(name.isNotEmpty() && job.isNotEmpty()){
                val map= mutableMapOf<String,String>()
                map["name"]=name.toString()
                map["job"]=job.toString()

                RequestApi.posRequest("users",object : CallbackApi{
                    override fun onResponse(value: String?) {
                        Toast.makeText(this@CreateUser,"user ws created",Toast.LENGTH_SHORT).show()
                    }

                    override fun onFailure(value: String?) {
                    }

                },map,this)
            }
        }
    }
}
