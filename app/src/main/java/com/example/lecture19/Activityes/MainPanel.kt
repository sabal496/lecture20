package com.example.lecture19.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.lecture19.R
import kotlinx.android.synthetic.main.activity_main_panel.*

class MainPanel : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_panel)
        init()

    }

    private  fun  init(){
        signinbtn.setOnClickListener(){
            val intent= Intent(this, SignIn::class.java)
            startActivity(intent)

        }
        registerbtn.setOnClickListener(){

            val intent=Intent(this, register::class.java)
            startActivity(intent)

        }

        listusers.setOnClickListener(){
            val intent=Intent(this,listRecycle::class.java)
            startActivity(intent)
        }
        createbtn.setOnClickListener(){
            val intent=Intent(this,CreateUser::class.java)
            startActivity(intent)
        }
    }
}
