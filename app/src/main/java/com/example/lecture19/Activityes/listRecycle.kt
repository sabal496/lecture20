package com.example.lecture19.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lecture19.Adapters.Adapter
import com.example.lecture19.Api.RequestApi
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.Interfaces.callbackid
import com.example.lecture19.Models.MyModel
import com.example.lecture19.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_list_recycle.*
import org.json.JSONObject
import retrofit2.Response

class listRecycle : AppCompatActivity() {

    var datalist= mutableListOf<MyModel.data>()
    lateinit var  adapter:Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_recycle)

        init()
    }
    private  fun  init(){

         RequestApi.getRequest("unknown",object : CallbackApi {
             override fun onResponse(value: String?) {
                 val userm:MyModel= Gson().fromJson(value.toString(),MyModel::class.java)

                    val jsonobj=JSONObject(value)

                 if(jsonobj.has("data")){
                     val jsonarray=jsonobj.getJSONArray("data")
                     (0 until jsonarray.length()).forEach(){
                         it->
                         val datamodel=Gson().fromJson(jsonarray.getString(it),MyModel.data::class.java)
                         datalist.add(datamodel)
                     }

                 }
                 adapter=Adapter(datalist,object :callbackid{
                     override fun getid(pos: Int) {
                        var intent=Intent(this@listRecycle,Details::class.java)
                         intent.putExtra("id",pos)
                         startActivity(intent)
                     }
                 })
                 recycle.layoutManager=LinearLayoutManager(this@listRecycle)
                 recycle.adapter=adapter
             }

             override fun onFailure(value: String?) {
                 d("fff",value)
             }
         },this)


    }



}
