package com.example.lecture19.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.example.lecture19.Api.RequestApi
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.Interfaces.callbackid
import com.example.lecture19.Models.MyModel
import com.example.lecture19.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_details.*
import org.json.JSONObject

class Details : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        init()
    }

    private  fun  init(){
        val inte=intent.extras
        val id=inte?.getInt("id")

        RequestApi.getRequest("unknown",id,object :CallbackApi{
            override fun onResponse(value: String?) {
              lateinit var model:MyModel.data
              lateinit  var admodel:MyModel.ad

                val jsonobject=JSONObject(value)

                if(jsonobject.has("data")){
                    var jsondata=jsonobject.getString("data")
                      model=Gson().fromJson(jsondata,MyModel.data::class.java)

                }
                if(jsonobject.has("ad")){
                    var jsondata=jsonobject.getString("ad")
                    admodel=Gson().fromJson(jsondata,MyModel.ad::class.java)
                }

                company.setText(admodel.company)
                url.text = admodel.url
                text.text = admodel.text
                idd.text = model.id.toString()
                name.text = model.name
                pantonevalue.text = model.pantonevalue
                year.text = model.year.toString()



            }

            override fun onFailure(value: String?) {
            }
        },this)

    }

}
