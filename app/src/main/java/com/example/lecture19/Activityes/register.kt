package com.example.lecture19.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.R
import com.example.lecture19.Api.RequestApi
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.password
import kotlinx.android.synthetic.main.activity_register.username
import org.json.JSONObject
import retrofit2.Response

class register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    private fun init(){
        regbtn.setOnClickListener(){
            val email=username.text.toString()
            val password=password.text.toString()
            val confpassword=confirmpassword.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty() && password==confpassword){
                val map= mutableMapOf<String,String>()
                map["email"]=email
                map["password"]=password

                RequestApi.posRequest(
                    "register",
                    object : CallbackApi {
                        override fun onResponse(value: String?) {

                                        Toast.makeText(
                                            this@register,
                                            "ok",
                                            Toast.LENGTH_SHORT
                                        ).show()


                        }

                        override fun onFailure(value: String?) {
                        }

                    },
                    map,
                    this
                )
            }

        }
    }
}
