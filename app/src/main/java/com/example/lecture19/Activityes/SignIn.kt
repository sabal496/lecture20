package com.example.lecture19.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.example.lecture19.Interfaces.CallbackApi
import com.example.lecture19.R
import com.example.lecture19.Api.RequestApi
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import retrofit2.Response

class SignIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private  fun init(){


        signinbtn.setOnClickListener(){
            val username=username.text.toString()
            val password=password.text.toString()
            if(username.isNotEmpty() && password.isNotEmpty()){
                val map= mutableMapOf<String,String>()
                map["email"]=username
                map["password"]=password
                RequestApi.posRequest(
                    "login",
                    object : CallbackApi {
                        override fun onResponse(value: String?) {

                            val json = JSONObject(value )
                            if(json.has("error")){
                                Toast.makeText(this@SignIn,json.getString("error"),Toast.LENGTH_SHORT).show()

                            } else {

                                         val intent=Intent(this@SignIn,listRecycle::class.java)
                                            startActivity(intent)
                                        finish()
                                    }



                        }

                        override fun onFailure(value: String?) {

                        }
                    },
                    map,
                    this
                )

            }
        }


    }
}
