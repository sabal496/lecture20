package com.example.lecture19.Interfaces

import retrofit2.Response

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}